
# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [Unreleased]


## [1.1.0] - 2017-12-28
### Added

- window-get-window-property (xcontext)
- window-get-window-property* (display, window)
- window-get-class (xcontext)
- window-get-class* (display, window)
- window-find-top-level (xcontext)
- window-find-top-level* (display, window)


## [1.0.0] - 2017-12-17
### Added

- make-xcontext: xinerama-screen may be an index
- window-get-icons (xcontext)
- window-get-icons* (display, window)
- window-get-title (xcontext)
- window-get-title* (display, window)
- window-property-set* (display, window)
- window-property-append* (display, window)
- set-wm-protocols* (display, window)
- update-event-mask!

### Changed

- xcontext-xinerama-screen is now a screen number, not the info object.

### Fixed

- export xcontext-root
- add-event-handler!: bug fix concerning events with the same event type
- make-text-property

### Removed

- active-window-title (use get-active-window + window-get-title)


## [0.5.0] - 2017-06-05
### Added

- xinerama support
 
### Removed

- current-xcontext (unused)


## [0.4.3] - 2015-11-12
### Added

- number-of-desktops
- desktop-names
- window-get-string-property (xcontext)
- window-get-string-property* (display, window)

### Changed

- active-window-title check both `WM_NAME` and `_NET_WM_NAME`

### Fixed

- package dependencies
- handle-event ignore windows for which we don't have an xcontext


## [0.4.2] - 2015-08-12
### Added

- xclientmessageevent-data-b wrapper
- xclientmessageevent-data-s wrapper
- xclientmessageevent-data-l wrapper

### Removed

- clientmessage-l0


## [0.4.1] - 2015-08-12
### Added

- clientmessage-l0
- make-event-handler: mask may be #f


## [0.4] - 2015-08-03
## [0.3] - 2015-08-02
## [0.2] - 2015-07-30
## [0.1] - 2015-07-26

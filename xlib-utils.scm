;; Copyright 2015,2017 John J Foerch. All rights reserved.
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;;
;;    1. Redistributions of source code must retain the above copyright
;;       notice, this list of conditions and the following disclaimer.
;;
;;    2. Redistributions in binary form must reproduce the above copyright
;;       notice, this list of conditions and the following disclaimer in
;;       the documentation and/or other materials provided with the
;;       distribution.
;;
;; THIS SOFTWARE IS PROVIDED BY JOHN J FOERCH ''AS IS'' AND ANY EXPRESS OR
;; IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
;; DISCLAIMED. IN NO EVENT SHALL JOHN J FOERCH OR CONTRIBUTORS BE LIABLE
;; FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
;; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
;; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
;; OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
;; ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(module xlib-utils
    (;; xcontext api
     make-xcontext xcontext?
     xcontext-display xcontext-screen
     xcontext-xinerama-screen xcontext-window
     xcontext-root xcontext-data xcontext-data-set!
     with-xcontext

     ;; xcontext helpers
     %xcontext-accessor

     ;; event dispatching
     add-event-handler!
     handle-event
     update-event-mask!

     ;; event unpacking
     xclientmessageevent-data-b
     xclientmessageevent-data-s
     xclientmessageevent-data-l

     ;; screens
     screen-or-xinerama-screen-height
     screen-or-xinerama-screen-width
     screen-or-xinerama-screen-left
     screen-or-xinerama-screen-top

     ;; properties
     window-property-type
     window-property-format
     window-property-data
     window-property-count
     make-atom-property
     make-number-property
     make-numbers-property
     make-text-property
     window-property-set
     window-property-set*
     window-property-append
     window-property-append*
     window-get-string-property
     window-get-string-property*
     window-get-window-property
     window-get-window-property*
     window-get-class
     window-get-class*
     window-get-icons
     window-get-icons*
     window-get-title
     window-get-title*
     set-wm-protocols
     set-wm-protocols*
     window-find-top-level
     window-find-top-level*

     get-active-window

     ;; desktops
     number-of-desktops
     desktop-names
     switch-to-desktop)

(import chicken scheme foreign foreigners)

(use (srfi 1 4)
     data-structures
     lolevel
     matchable
     (only miscmacros begin0)
     xinerama
     (rename xlib
             (xclientmessageevent-data-b
              %xclientmessageevent-data-b)
             (xclientmessageevent-data-s
              %xclientmessageevent-data-s)
             (xclientmessageevent-data-l
              %xclientmessageevent-data-l)))

(foreign-declare "#include <X11/Xlib.h>")


(define (xlib-utils-error loc msg . args)
  (abort (make-composite-condition
	   (make-property-condition 'exn
				    'location loc
				    'message msg
				    'arguments args)
	   (make-property-condition 'xlib-utils))))


;;;
;;; Xcontext
;;;

(define-record-type :xcontext
  (%%make-xcontext display screen xinerama-screen window event-handlers)
  xcontext?
  (display xcontext-display xcontext-display-set!)
  (screen xcontext-screen xcontext-screen-set!)
  (xinerama-screen xcontext-xinerama-screen xcontext-xinerama-screen-set!)
  (window xcontext-window xcontext-window-set!)
  (event-handlers xcontext-event-handlers xcontext-event-handlers-set!)
  (data xcontext-data xcontext-data-set!))

(define (xcontext-populate xc #!key display screen xinerama-screen window)
  (when display (xcontext-display-set! xc display))
  (when screen (xcontext-screen-set! xc screen))
  (when xinerama-screen (xcontext-xinerama-screen-set! xc xinerama-screen))
  (when window (xcontext-window-set! xc window))
  (when (and (xcontext-display xc) (not (xcontext-screen xc)))
    (xcontext-screen-set! xc (xdefaultscreen display)))
  (when (and (xcontext-display xc) (xcontext-screen xc)
             (not (xcontext-window xc)))
    (xcontext-window-set! xc (xrootwindow display (xcontext-screen xc))))
  xc)

(define %make-xcontext
  (case-lambda
   ((display screen xinerama-screen window)
    (%%make-xcontext display screen xinerama-screen window #f))
   (()
    (%%make-xcontext #f #f #f #f #f))))

(define (xcontext-clone xc)
  (%%make-xcontext
   (xcontext-display xc)
   (xcontext-screen xc)
   (xcontext-xinerama-screen xc)
   (xcontext-window xc)
   #f))

(define make-xcontext
  (match-lambda*
   (() (%make-xcontext))
   (((? keyword? k) . rest)
    (apply xcontext-populate (%make-xcontext) k rest))
   ((xc (? keyword? k) . rest)
    (apply xcontext-populate (xcontext-clone xc) k rest))))

(define (xcontext-root xc)
  (xrootwindow (xcontext-display xc)
               (xcontext-screen xc)))

(define (%xcontext-accessor field)
  (alist-ref field `((xcontext . ,identity)
                     (display . ,xcontext-display)
                     (screen . ,xcontext-screen)
                     (xinerama-screen . ,xcontext-xinerama-screen)
                     (window . ,xcontext-window)
                     (data . ,xcontext-data)
                     (root . ,xcontext-root))))

(define-syntax with-xcontext
  (syntax-rules ()
    ((with-xcontext xc (field ...) form . forms)
     (let* ((_xc xc) ;; evaluate xc only once
            (field ((%xcontext-accessor 'field) _xc))
            ...)
       form . forms))))


;;;
;;; Event Dispatching
;;;

(define-record-type :event-handler
  (%make-event-handler handler mask guard)
  event-handler?
  (handler event-handler-handler)
  (mask event-handler-mask)
  (guard event-handler-guard))

(define (make-event-handler handler mask guard)
  (let ((mask (cond
               ((pair? mask)
                (apply bitwise-ior mask))
               ((eq? #f mask) 0)
               (else mask))))
    (%make-event-handler handler mask guard)))

(define (add-event-handler! xcontext event-type mask handler guard)
  (let* ((handler (make-event-handler handler mask guard))
         (added? #f)
         (new-event-list
          (fold
           (lambda (x h)
             (cons (if (= event-type (first x))
                       (begin
                         (set! added? #t)
                         (cons* event-type handler (cdr x)))
                       x)
                   h))
           '()
           (or (xcontext-event-handlers xcontext) (list)))))
    (xcontext-event-handlers-set!
     xcontext (if added?
                  new-event-list
                  (cons (list event-type handler)
                        new-event-list)))))

(define (handle-event event xcontexts)
  (let* ((window (xevent-xany-window event))
         (event-type (xevent-type event)))
    (and-let* ((target-xc (find (lambda (xc) (= window (xcontext-window xc)))
                                xcontexts))
               (handlers (xcontext-event-handlers target-xc))
               (handlers (alist-ref event-type handlers)))
      (for-each
       (lambda (handler)
         (when ((or (event-handler-guard handler) identity) event)
           ((event-handler-handler handler) target-xc event)))
       handlers))))

(define (update-event-mask! xcontext . additional-masks)
  (with-xcontext xcontext (display window)
    (xselectinput display window
                  (apply bitwise-ior
                         (fold
                          (match-lambda*
                           (((event-type . hs) mask)
                            (apply bitwise-ior mask (map event-handler-mask hs))))
                          0
                          (or (xcontext-event-handlers xcontext) '()))
                         additional-masks))))


;;;
;;; Event Unpacking
;;;

(define (xclientmessageevent-data-b event)
  (let ((data (%xclientmessageevent-data-b event)))
    (list-tabulate
     20
     (lambda (i)
       ((foreign-lambda* char (((c-pointer char) data) (int i))
          "C_return(data[i]);")
        data i)))))

(define (xclientmessageevent-data-s event)
  (let ((data (%xclientmessageevent-data-s event)))
    (list-tabulate
     10
     (lambda (i)
       ((foreign-lambda* short (((c-pointer short) data) (int i))
          "C_return(data[i]);")
        data i)))))

(define (xclientmessageevent-data-l event)
  (let ((data (%xclientmessageevent-data-l event)))
    (list-tabulate
     5
     (lambda (i)
       ((foreign-lambda* long (((c-pointer long) data) (int i))
          "C_return(data[i]);")
        data i)))))


;;;
;;; Screens
;;;

;;XXX: we no longer store the xinerama-screen-info, because it may change
;;     when screens are reconfigured.  that creates inefficiency with
;;     these lookup procedures.
(define (screen-or-xinerama-screen-height xcontext)
  (with-xcontext xcontext (display screen xinerama-screen)
    (if xinerama-screen
        (xinerama-screen-info-height
         (list-ref (xinerama-query-screens display)
                   xinerama-screen))
        (xdisplayheight display screen))))

(define (screen-or-xinerama-screen-width xcontext)
  (with-xcontext xcontext (display screen xinerama-screen)
    (if xinerama-screen
        (xinerama-screen-info-width
         (list-ref (xinerama-query-screens display)
                   xinerama-screen))
        (xdisplaywidth display screen))))

(define (screen-or-xinerama-screen-left xcontext)
  (with-xcontext xcontext (display xinerama-screen)
    (if xinerama-screen
        (xinerama-screen-info-x-org
         (list-ref (xinerama-query-screens display)
                   xinerama-screen))
        0)))

(define (screen-or-xinerama-screen-top xcontext)
  (with-xcontext xcontext (display xinerama-screen)
    (if xinerama-screen
        (xinerama-screen-info-y-org
         (list-ref (xinerama-query-screens display)
                   xinerama-screen))
        0)))


;;;
;;; Window Property Utils
;;;

#>
int xerrorhandler_ignore_errors (Display* d, XErrorEvent* e) { return 0; }
<#

(define-record window-property
  type format data count)

(define (make-atom-property xcontext atom-name)
  (let* ((display (xcontext-display xcontext))
         (data (xinternatom display atom-name 0)))
    (let-location ((data unsigned-long data))
      (make-window-property "ATOM" 32
                            (location data)
                            1))))

(define (make-number-property number)
  (let-location ((data unsigned-long number))
    (make-window-property "CARDINAL" 32 (location data) 1)))

(define (make-numbers-property numbers)
  (let* ((vec (list->u32vector numbers))
         (len (u32vector-length vec))
         (lvec ((foreign-lambda* c-pointer ((u32vector s) (int length))
                  "unsigned long * lvec = malloc(sizeof(unsigned long) * length);"
                  "int i;"
                  "for (i = 0; i < length; i++) {"
                  "    lvec[i] = s[i];"
                  "}"
                  "C_return(lvec);")
                vec len)))
    (set-finalizer! lvec free)
    (make-window-property "CARDINAL" 32 lvec len)))

(define (make-text-property s)
  (let* ((b (blob->u8vector/shared
             (string->blob s)))
         (l (u8vector-length b))
         (p (make-xtextproperty))
         (t ((foreign-lambda* c-pointer ((u8vector b) (int l))
               "unsigned char *t = malloc(l);"
               "memcpy(t, b, l);"
               "C_return(t);") b l)))
    (set-xtextproperty-value! p t)
    (set-xtextproperty-encoding! p XA_STRING)
    (set-xtextproperty-format! p 8)
    (set-xtextproperty-nitems! p l)
    p))

(define (window-property-set* display window key value)
  (xchangeproperty display window
                   (xinternatom display key 0)
                   (xinternatom display (window-property-type value) 0)
                   (window-property-format value)
                   PROPMODEREPLACE
                   (window-property-data value)
                   (window-property-count value)))

(define (window-property-set xcontext key value)
  (with-xcontext xcontext (display window)
    (window-property-set* display window key value)))

(define (window-property-append* display window key value)
  (xchangeproperty display window
                   (xinternatom display key 0)
                   (xinternatom display (window-property-type value) 0)
                   (window-property-format value)
                   PROPMODEAPPEND
                   (window-property-data value)
                   (window-property-count value)))

(define (window-property-append xcontext key value)
  (with-xcontext xcontext (display window)
    (window-property-append* display window key value)))

(define (window-get-string-property* display window property)
  (let ((property (xinternatom display property 0)))
    (let-location ((xa_ret_type unsigned-long)
                   (ret_format int)
                   (ret_nitems unsigned-long)
                   (ret_bytes_after unsigned-long)
                   (ret_data unsigned-c-string*))
      ((foreign-lambda* void () "XSetErrorHandler(xerrorhandler_ignore_errors);"))
      (let ((status (xgetwindowproperty display window property
                                        0 #x77777777 0
                                        ANYPROPERTYTYPE
                                        (location xa_ret_type)
                                        (location ret_format)
                                        (location ret_nitems)
                                        (location ret_bytes_after)
                                        (location ret_data))))
        ((foreign-lambda* void () "XSetErrorHandler(NULL);"))
        (if (= SUCCESS status)
            ret_data
            #f)))))

(define (window-get-string-property xcontext property)
  (with-xcontext xcontext (display window)
    (window-get-string-property display window property)))

(define (window-get-window-property* display window prop)
  (let ((property (xinternatom display prop 0)))
    (let-location ((xa_ret_type unsigned-long)
                   (ret_format int)
                   (ret_nitems unsigned-long)
                   (ret_bytes_after unsigned-long)
                   (ret_window_id unsigned-c-string*))
      (xgetwindowproperty display window property
                          0 4 0
                          ANYPROPERTYTYPE
                          (location xa_ret_type)
                          (location ret_format)
                          (location ret_nitems)
                          (location ret_bytes_after)
                          (location ret_window_id))
      (if (= NONE xa_ret_type) ;; property did not exist
          #f
          (let ((window-id
                 ((foreign-lambda* unsigned-long ((c-pointer data))
                    "unsigned long** longs = (unsigned long**)data;"
                    "C_return(longs[0][0]);")
                  (location ret_window_id))))
            (if (= NONE window-id)
                #f
                window-id))))))

(define (window-get-window-property xcontext prop)
  (with-xcontext xcontext (display window)
    (window-get-window-property* display window prop)))

(define (window-get-class* display window)
  (let* ((classhint (xallocclasshint))
         (status (xgetclasshint display window classhint)))
    (define cstring
      (foreign-lambda* c-string ((c-pointer p))
        "char *s = (char*)p;"
        "C_return(s);"))
    (begin0
     (if (zero? status)
         #f
         (let* ((res_name (xclasshint-res_name classhint))
                (res_class (xclasshint-res_class classhint))
                (name (cstring res_name))
                (class (cstring res_class)))
           (xfree res_name)
           (xfree res_class)
           (list name class)))
     (xfree classhint))))

(define (window-get-class xcontext)
  (with-xcontext xcontext (display window)
    (window-get-class* display window)))

(define (window-get-icons* display screen window)
  (define (parse-icons data data-size visual)
    (let-location ((width unsigned-long)
                   (height unsigned-long))
      (reverse!
       (let loop ((offset 0)
                  (icons (list)))
         (cond
          ((< offset data-size)
           (let ((imgdata
                  ((foreign-lambda* c-pointer
                       (((c-pointer unsigned-long) data)
                        (unsigned-long data_size)
                        (unsigned-long offset)
                        ((c-pointer unsigned-long) width_out)
                        ((c-pointer unsigned-long) height_out))
                     "unsigned int *dstdata = NULL;"
                     "unsigned long width = *(data + offset);"
                     "unsigned long height = *(data + offset + 1);"
                     "unsigned long size = width * height;"
                     "int i;"
                     "if (size <= data_size - offset - 2) {"
                     "    dstdata = malloc(size * 4);"
                     "    for (i = 0; i < size; ++i) {"
                     "        dstdata[i] = *((unsigned int*)(data + (offset + 2 + i)));"
                     "    }"
                     "}"
                     "*width_out = width;"
                     "*height_out = height;"
                     "C_return(dstdata);")
                   data data-size offset
                   (location width) (location height))))
             (if imgdata
                 (let ((ximage (xcreateimage display visual 32 ZPIXMAP
                                             0 imgdata width height 8 0)))
                   (set-finalizer! ximage xdestroyimage)
                   (loop (+ offset 2 (* width height))
                         (cons ximage icons)))
                 icons)))
          (else icons))))))
  ;; entry point
  (let-location ((xa_ret_type unsigned-long)
                 (ret_format int)
                 (ret_nitems unsigned-long)
                 (ret_bytes_after unsigned-long)
                 (ret_data (c-pointer unsigned-char)))
    ((foreign-lambda* void () "XSetErrorHandler(xerrorhandler_ignore_errors);"))
    (let* ((property (xinternatom display "_NET_WM_ICON" 0))
           (status (xgetwindowproperty display window property
                                       0 #x77777777 0
                                       XA_CARDINAL
                                       (location xa_ret_type)
                                       (location ret_format)
                                       (location ret_nitems)
                                       (location ret_bytes_after)
                                       (location ret_data))))
      ((foreign-lambda* void () "XSetErrorHandler(NULL);"))
      (if (and (= SUCCESS status) ret_data)
          (begin0
           (let* ((v-info (make-xvisualinfo))
                  (have-visual? (xmatchvisualinfo display screen 32 TRUECOLOR v-info))
                  (visual (xvisualinfo-visual v-info)))
             (begin0
              (if have-visual?
                  (parse-icons ret_data ret_nitems visual)
                  (xlib-utils-error 'window-get-icons* "Failed to get appropriate Visual"))
              (xfree v-info)))
           (xfree ret_data))
          #f))))

(define (window-get-icons xcontext)
  (with-xcontext xcontext (display screen window)
    (window-get-icons* display screen window)))

(define (window-get-title* display window)
  (or (window-get-string-property* display window "_NET_WM_NAME")
      (window-get-string-property* display window "WM_NAME")))

(define (window-get-title xcontext)
  (with-xcontext xcontext (display window)
    (window-get-title* display window)))

(define (set-wm-protocols* display window syms)
  (let* ((atoms (map (lambda (a)
                       (xinternatom display (symbol->string a) 1))
                     syms))
         (vec (list->u32vector atoms))
         (len (u32vector-length vec))
         (lvec ((foreign-lambda* c-pointer ((u32vector s) (int length))
                  "unsigned long * lvec = malloc(sizeof(unsigned long) * length);"
                  "int i;"
                  "for (i = 0; i < length; i++) {"
                  "    lvec[i] = s[i];"
                  "}"
                  "C_return(lvec);")
                vec len)))
    (set-finalizer! lvec free)
    (xsetwmprotocols display window lvec len)))

(define (set-wm-protocols xcontext syms)
  (with-xcontext xcontext (display window)
    (set-wm-protocols* display window syms)))

(define (window-find-top-level* display window)
  (let-location ((root unsigned-long)
                 (parent unsigned-long)
                 (children (c-pointer unsigned-long))
                 (nchildren unsigned-int))
    (let loop ((window window))
      (let ((leader (window-get-window-property* display window "WM_CLIENT_LEADER")))
        ;;XXX: also WM_TRANSIENT_FOR?
        (if leader
            leader
            (let ((status (xquerytree display window (location root)
                                      (location parent) (location children)
                                      (location nchildren))))
              (cond
               ((zero? status) window)
               (else
                (when children
                  (xfree children))
                (if (= root parent)
                    window
                    (loop parent))))))))))

(define (window-find-top-level xcontext)
  (with-xcontext xcontext (display window)
    (window-find-top-level* display window)))


;;;
;;; Active Window
;;;

(define (get-active-window xcontext)
  (with-xcontext xcontext (display screen root)
    (window-get-window-property* display root "_NET_ACTIVE_WINDOW")))


;;;
;;; Desktops
;;;

(define (number-of-desktops xcontext)
  (with-xcontext xcontext (display screen root)
    (let ((property (xinternatom display "_NET_NUMBER_OF_DESKTOPS" 0)))
      (let-location ((xa_ret_type unsigned-long)
                     (ret_format int)
                     (ret_nitems unsigned-long)
                     (ret_bytes_after unsigned-long)
                     (ret_ndesktops unsigned-long))
        (xgetwindowproperty display root property
                            0 1 0
                            XA_CARDINAL
                            (location xa_ret_type)
                            (location ret_format)
                            (location ret_nitems)
                            (location ret_bytes_after)
                            (location ret_ndesktops))
        (assert (not (= NONE xa_ret_type)))
        ((foreign-lambda* unsigned-long ((c-pointer data))
           "unsigned long** longs = (unsigned long**)data;"
           "C_return(longs[0][0]);")
         (location ret_ndesktops))))))

(define (desktop-names xcontext)
  (with-xcontext xcontext (display screen root)
    (let ((ndesktops (number-of-desktops xcontext))
          (MAX_PROPERTY_VALUE_LEN 4096)
          (property (xinternatom display "_NET_DESKTOP_NAMES" 0))
          (req_type (xinternatom display "UTF8_STRING" 0)))
      (let-location ((xa_ret_type unsigned-long)
                     (ret_format int)
                     (ret_nitems unsigned-long)
                     (ret_bytes_after unsigned-long)
                     (ret_prop unsigned-c-string*))
        (xgetwindowproperty display root property
                            0 (/ MAX_PROPERTY_VALUE_LEN 4) 0
                            req_type
                            (location xa_ret_type)
                            (location ret_format)
                            (location ret_nitems)
                            (location ret_bytes_after)
                            (location ret_prop))
        (assert (= req_type xa_ret_type))
        ((foreign-lambda* c-string-list (((c-pointer unsigned-c-string) names)
                                         (unsigned-long nitems)
                                         (unsigned-long ndesktops))
           ;; if there are more names than ndesktops, the excess names are
           ;; reserved for future desktops, so should not be included in
           ;; the result.

           ;; if there are fewer names than ndesktops, the remainder are
           ;; unnamed.
           "unsigned char** result = malloc(sizeof(unsigned char*) * (ndesktops + 1));"
           "int i, d = 0, atstart = 1;"
           "for (i = 0; i < nitems; i++) {"
           "    if (d >= ndesktops)"
           "        break;"
           "    if (atstart) {"
           "        printf(\"%d %s\\n\", d, &names[0][i]);"
           "        result[d] = &names[0][i];"
           "        atstart = 0;"
           "    }"
           "    if (names[0][i] == 0) {"
           "        atstart = 1;"
           "        d++;"
           "    }"
           "}"
           "for (i = d; i < ndesktops; i++) {"
           "    printf(\"%d <no name>\\n\", i);"
           ;; "    result[i] = &result[ndesktops];"
           ;; these need to point at some empty string, but not be a NULL pointer
           "}"
           "printf(\"list terminator: %d\\n\", ndesktops);"
           "result[ndesktops] = 0;"
           "C_return(result);")
         (location ret_prop) ret_nitems ndesktops)))))

(define (switch-to-desktop/number display screen desktop)
  (let ((root (xrootwindow display screen))
        (event-mask (bitwise-ior SUBSTRUCTURENOTIFYMASK
                                 SUBSTRUCTUREREDIRECTMASK))
        (event (make-xclientmessageevent)))
    (set-xclientmessageevent-type! event CLIENTMESSAGE)
    (set-xclientmessageevent-serial! event 0)
    (set-xclientmessageevent-send_event! event 1)
    (set-xclientmessageevent-display! event display)
    (set-xclientmessageevent-window! event root)
    (set-xclientmessageevent-message_type!
     event (xinternatom display "_NET_CURRENT_DESKTOP" 0))
    (set-xclientmessageevent-format! event 32)
    (define make-event-data-l
      (foreign-lambda* void (((c-pointer long) data_l)
                             (long l0) (long l1) (long l2)
                             (long l3) (long l4))
        "data_l[0] = l0; data_l[1] = l1;"
        "data_l[2] = l2; data_l[3] = l3;"
        "data_l[4] = l4;"))
    (make-event-data-l (%xclientmessageevent-data-l event)
                       desktop (current-seconds) 0 0 0)
    (xsendevent display root 0 event-mask event)))

(define (switch-to-desktop xcontext desktop)
  (with-xcontext xcontext (display screen)
    (cond
     ((number? desktop) (switch-to-desktop/number display screen desktop))
     ((string? desktop)
      (let ((root (xrootwindow display screen))
            (MAX_PROPERTY_VALUE_LEN 4096)
            (property (xinternatom display "_NET_DESKTOP_NAMES" 0))
            (req_type (xinternatom display "UTF8_STRING" 0)))
        (let-location ((xa_ret_type unsigned-long)
                       (ret_format int)
                       (ret_nitems unsigned-long)
                       (ret_bytes_after unsigned-long)
                       (ret_prop unsigned-c-string*))
          (xgetwindowproperty display root property
                              0 (/ MAX_PROPERTY_VALUE_LEN 4) 0
                              req_type
                              (location xa_ret_type)
                              (location ret_format)
                              (location ret_nitems)
                              (location ret_bytes_after)
                              (location ret_prop))
          (assert (= req_type xa_ret_type))
          (define find-desktop
            (foreign-lambda* int ((unsigned-c-string target)
                                  ((c-pointer unsigned-c-string) names)
                                  (unsigned-long nitems))
              "int i, d = 0, atstart = 1;"
              "for (i = 0; i < nitems; i++) {"
              "    if (atstart) {"
              "        if (0 == strcmp(target, &names[0][i]))"
              "            C_return(d);"
              "        atstart = 0;"
              "    }"
              "    if (names[0][i] == 0) {"
              "        atstart = 1;"
              "        d++;"
              "    }"
              "}"
              "C_return(-1);"))
          (let ((desktop-number (find-desktop desktop (location ret_prop) ret_nitems)))
            (when (> desktop-number -1)
              (switch-to-desktop/number display screen desktop-number)))))))))

)

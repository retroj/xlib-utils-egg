
(load "xlib-utils")
(import xlib-utils)

(describe "%xcontext-accessor"
  (it "takes xcontext"
      (expect (%xcontext-accessor 'xcontext)
              (be procedure?)))
  (it "takes display"
      (expect (%xcontext-accessor 'display)
              (be procedure?)))
  (it "takes screen"
      (expect (%xcontext-accessor 'screen)
              (be procedure?)))
  (it "takes window"
      (expect (%xcontext-accessor 'window)
              (be procedure?)))
  (it "takes root"
      (expect (%xcontext-accessor 'root)
              (be procedure?))))

(describe "with-xcontext"
  (it "has field xcontext"
      (let ((xc (make-xcontext)))
        (expect (with-xcontext xc (xcontext) xcontext)
                (be xc))))

  (it "does not error when nested"
      (do-not
       (expect
        (with-xcontext (make-xcontext) (xcontext)
                       (with-xcontext xcontext (xcontext) xcontext))
        (raise error))))

  (it "works as expected when nested wrt lexical scope"
      (expect
       (with-xcontext (make-xcontext window: 3) (window)
                      (with-xcontext (make-xcontext window: 4) (window)
                                     window))
       (be 4))))

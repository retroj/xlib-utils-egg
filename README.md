
# xlib-utils

## Description

Grab-bag of utilities for use with the xlib egg.  This includes a set of
procedures for working with X properties, events, and EWMH desktops, as
well as a datatype called xcontext, which is a way to package up a
display, a screen, a xinerama-screen, and a window into a single
convenient record.

For bug reports, feature requests, and development versions, visit the
[bitbucket project page](https://bitbucket.org/retroj/xlib-utils-egg/).

## Authors

* John J Foerch


## Requirements
### Chicken Eggs

* foreigners
* matchable
* miscmacros
* xinerama
* xlib

### C Headers

* Xlib.h


## API
### xcontext

* **(make-xcontext) => xcontext**
* **(make-xcontext . keywords) => xcontext**
* **(make-xcontext xc . keywords) => xcontext**
* **(xcontext? xc) => bool**
* **(xcontext-display xc) => display or #f**
* **(xcontext-screen xc) => fixnum or #f**
* **(xcontext-xinerama-screen xc) => xinerama-screen-info or #f**
* **(xcontext-window xc) => window or #f**
* **(xcontext-root xc) => window or #f**
* **(xcontext-data xc) => object**
* **(xcontext-data-set! xc object) => undefined**
* **(with-xcontext xc (field...) body...) => body...**

### events

* **(add-event-handler! xc event-type mask handler guard) => undefined**
* **(handle-event event xcs) => undefined**

* **(xclientmessageevent-data-b event) => (fixnum...)**
* **(xclientmessageevent-data-s event) => (fixnum...)**
* **(xclientmessageevent-data-l event) => (fixnum...)**

### screens

* **(screen-or-xinerama-screen-height xc) => fixnum**
* **(screen-or-xinerama-screen-width xc) => fixnum**
* **(screen-or-xinerama-screen-left xc) => fixnum**
* **(screen-or-xinerama-screen-top xc) => fixnum**

### properties

* **(window-property-type) => **
* **(window-property-format) => **
* **(window-property-data) => **
* **(window-property-count) => **
* **(make-atom-property xc atom-name) => window-property**
* **(make-number-property number) => window-property**
* **(make-numbers-property numbers) => window-property**
* **(make-text-property text) => text-property**
* **(window-property-set xc key value) => undefined**
* **(window-property-append xc key value) => undefined**
* **(window-get-string-property display window property) => string or #f**
* **(window-get-string-property* xc property) => string or #f**
* **(set-wm-protocols xc syms) => undefined**
* **(active-window-title xc) => string or #f**

### desktops

* **(number-of-desktops xc) => fixnum**
* **(desktop-names xc) => (string...)**
* **(switch-to-desktop xc desktop-name) => undefined**


## Examples

```scheme
```


## License

BSD


## Version History

* 0.1 (2015-07-26) utils for properties and desktops
* 0.2 (2015-07-30) xcontext introduced
* 0.3 (2015-08-02) with-xcontext root, active-window-title, set-wm-protocols
* 0.4 (2015-08-03) xcontext-data, xcontext-event-handlers
* 0.4.1 (2015-08-15) clientmessage-l0
* 0.4.2 (2015-08-15) general event data unpacking
* 0.4.3 (2015-11-12) window-get-string-property, window-get-string-property*, number-of-desktops, desktop-names, misc fixes
* 0.5.0 (2017-06-05) xinerama support
